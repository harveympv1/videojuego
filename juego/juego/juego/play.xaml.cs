﻿using juego.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class play : ContentPage
	{
        private const string UrlRoot = "https://harvey-gisel-videojuego.herokuapp.com/";
        private const string UrlListContact = UrlRoot + "play";
        private const string UrlDeleteContact = UrlRoot + "play";
        private readonly HttpClient client = new HttpClient();
        private ObservableCollection<Play> _contact;

        public play()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Application.Current.Properties.ContainsKey("id_user"))
            {
                ListDataContacts();
            }
            else
            {

            }
        }

        // Metodo para listar todos los contactos
        async public void ListDataContacts()
        {
            string contentplay = await client.GetStringAsync(UrlListContact);
            List<Play> contacts = JsonConvert.DeserializeObject<List<Play>>(contentplay);
            _contact = new ObservableCollection<Play>(contacts);

            // new ObservableCollection<Play>(contacts);
            listViewContacts.ItemsSource = _contact;
        }
        async private void creaplay()
        {
            await Navigation.PushModalAsync(new createplay());
        }
        public void ClickDeleteContact(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DeleteContact(mi.CommandParameter.ToString());
        }
        async public void DeleteContact(string position)
        {
            HttpResponseMessage response = null;
            response = await client.DeleteAsync(UrlDeleteContact + "/" + position);

            ListDataContacts();
        }

    }
}