﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class menu : ContentPage
	{
		public menu ()
		{
			InitializeComponent ();
		}
        async private void ClickButtonxbox(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new xbox());
        }
        async private void ClickButtonnintendo(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new nintendo());
        }
        async private void ClickButtonplay(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new play());
        }
      
    }
}