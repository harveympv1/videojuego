﻿using juego.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Updatenin : ContentPage
	{
        private const string UrlRoot = "https://harvey-gisel-videojuego.herokuapp.com/";
        private const string UrlUpdateContact = UrlRoot + "nintendo";
        private readonly HttpClient client = new HttpClient();
        private Nintendo contact;

        public Updatenin()
        {
            InitializeComponent();
            this.contact = contact;
            BindingContext = contact;
        }
        async public void ClickButtonUpdateContact(object sender, EventArgs e)
        {
            contact.Name = entryName.Text;
            contact.Descriptio = entryDes.Text;
            contact.Precio = entryprecio.Text;

            var json = JsonConvert.SerializeObject(contact);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = null;
            response = await client.PutAsync(UrlUpdateContact + "/" + contact.Id, content);

            await Navigation.PushModalAsync(new xbox());
        }
    }
}