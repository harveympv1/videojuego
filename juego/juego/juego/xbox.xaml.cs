﻿using juego.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class xbox : ContentPage
	{
        private const string UrlRoot = "https://harvey-gisel-videojuego.herokuapp.com/";
        private const string UrlListContact = UrlRoot + "xbox";
        private const string UrlDeleteContact = UrlRoot + "xbox";
        private readonly HttpClient client = new HttpClient();
        private ObservableCollection<Xbox> _contact;

        public xbox ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Application.Current.Properties.ContainsKey("id_user"))
            {
                ListDataContacts();
            }
            else
            {
                
            }
        }

        // Metodo para listar todos los contactos
        async public void ListDataContacts()
        {
            string content = await client.GetStringAsync(UrlListContact);
            List<Xbox> contacts = JsonConvert.DeserializeObject<List<Xbox>>(content);
            _contact = new ObservableCollection<Xbox>(contacts);
            listViewContacts.ItemsSource = _contact;
        }
        async private void creaxbox()
        {
            await Navigation.PushModalAsync(new Createxbox());
        }
        public void ClickDeleteContact(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DeleteContact(mi.CommandParameter.ToString());
        }
        async public void DeleteContact(string position)
        {
            HttpResponseMessage response = null;
            response = await client.DeleteAsync(UrlDeleteContact + "/" + position);

            ListDataContacts();
        }
        public void ClickUpdateContact(object sender, EventArgs e)
        {
            var mi = sender as MenuItem;
            var item = mi.BindingContext as Xbox;

            Xbox contact = new Xbox()
            {
                Id = item.Id,
                Name = item.Name,
                Descriptio = item.Descriptio,
                Precio = item.Precio,
                Image = item.Image
            };


            showWindowUpdateContact(contact);
        }

        async public void showWindowUpdateContact(Xbox contact)
        {
            await Navigation.PushModalAsync(new Updatexbox(contact));
        }


    }
}