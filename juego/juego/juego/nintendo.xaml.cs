﻿using juego.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class nintendo : ContentPage
    {
        private const string UrlRoot = "https://harvey-gisel-videojuego.herokuapp.com/";
        private const string UrlListContact = UrlRoot + "nintendo";
        private const string UrlDeleteContact = UrlRoot + "nintendo";
        private readonly HttpClient client = new HttpClient();
        private ObservableCollection<Nintendo> _contact;

        public nintendo()
        {
            InitializeComponent();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Application.Current.Properties.ContainsKey("id_user"))
            {
                ListDataContacts();
            }
            else
            {

            }
        }

        // Metodo para listar todos los contactos
        async public void ListDataContacts()
        {
            string content = await client.GetStringAsync(UrlListContact);
            List<Nintendo> contacts = JsonConvert.DeserializeObject<List<Nintendo>>(content);
            _contact = new ObservableCollection<Nintendo>(contacts);
            listViewContacts.ItemsSource = _contact;
        }
        async private void creanin()
        {
            await Navigation.PushModalAsync(new createnintendoxaml());
        }

        public void ClickDeleteContact(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            DeleteContact(mi.CommandParameter.ToString());
        }
        async public void DeleteContact(string position)
        {
            HttpResponseMessage response = null;
            response = await client.DeleteAsync(UrlDeleteContact + "/" + position);

            ListDataContacts();
        }
    }
}