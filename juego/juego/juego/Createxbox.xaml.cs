﻿using juego.models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace juego
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Createxbox : ContentPage
	{
        private const string UrlRoot = "https://harvey-gisel-videojuego.herokuapp.com/";
        private const string UrlCreateUser = UrlRoot + "createxbox";
        private readonly HttpClient client = new HttpClient();

        public Createxbox ()
		{
			InitializeComponent ();
		}
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Application.Current.Properties.ContainsKey("id_user"))
            {
                
            }
        }

        public void ClickButtonCreateUser(object sender, EventArgs e)
        {
            CreateAccount();
        }

        async public void CreateAccount()
        {
            Xbox user = new Xbox()
            {
                Precio = entryPrecio.Text,
                Name = entryName.Text,
                Descriptio = entryDes.Text
            };

            var json = JsonConvert.SerializeObject(user);
            var content = new StringContent(json, Encoding.UTF8, "application/json");

            HttpResponseMessage response = await client.PostAsync(UrlCreateUser, content);
            string message = await response.Content.ReadAsStringAsync();
            List<User> users = JsonConvert.DeserializeObject<List<User>>(message);

            Application.Current.Properties["id_user"] = users[0].Id;

            await Navigation.PushModalAsync(new menu());
        }
        
    }
}